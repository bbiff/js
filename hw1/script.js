let poems = [
    `
    From too much love of living<br>
    From hope and fear set free,<br>
    We thank with brief thanksgiving<br>
    Whatever gods may be<br>
    That no life lives for ever;<br>
    That dead men rise up never;<br>
    That even the weariest river<br>
    Winds somewhere safe to sea.<br>
    <br><br>
    Then star nor sun shall waken,<br>
    Nor any change of light:<br>
    Nor sound of waters shaken,<br>
    Nor any sound or sight:<br>
    Nor wintry leaves nor vernal,<br>
    Nor days nor things diurnal;<br>
    Only the sleep eternal<br>
    In an eternal night.<br>
    `,
    `
    We have plenty of matches in our house<br>
    We keep them on hand always<br>
    Currently our favourite brand<br>
    Is Ohio Blue Tip<br>
    Though we used to prefer Diamond Brand<br>
    That was before we discovered<br>
    Ohio Blue Tip matches<br>
    They are excellently packaged<br>
    Sturdy little boxes<br>
    With dark and light blue and white labels<br>
    With words lettered<br>
    In the shape of a megaphone<br>
    As if to say even louder to the world<br>
    Here is the most beautiful match in the world<br>
    It's one-and-a-half-inch soft pine stem<br>
    Capped by a grainy dark purple head<br>
    So sober and furious and stubbornly ready<br>
    To burst into flame<br>
    Lighting, perhaps the cigarette of the woman you love<br>
    For the first time<br>
    And it was never really the same after that<br>
    All this will we give you<br>
    That is what you gave me<br>
    I become the cigarette and you the match<br>
    Or I the match and you the cigarette<br>
    Blazing with kisses that smoulder towards heaven.<br>
    `,
    `
    I'm in the house.<br>
    It's nice out: warm<br>
    sun on cold snow.<br>
    First day of spring<br>
    or last of winter.<br>
    My legs run down<br>
    the stairs and out<br>
    the door, my top<br>
    half is here typing<br>
    `,
    `
    When you're a child you learn there are three dimensions<br>
    Height, width and depth<br>
    Like a shoebox<br>
    Then later you hear there's a fourth dimension<br>
    Time<br>
    Hmm<br>
    Then some say there can be five, six, seven…<br>

    I knock off work<br>
    Have a beer at the bar<br>
    I look down at the glass and feel glad<br>
    `,
    `
    Water falls from bright air.<br>
    It falls like hair, falling across a young girl's shoulders.<br>
    Water falls making pools in the asphalt, dirty mirrors with clouds and buildings inside.<br>
    It falls on the roof of my house.<br>
    It falls on my mother and on my hair.<br>
    Most people call it rain.<br>
    `
]


function WritePoem(poem_num) {
    document.getElementById("poem").innerHTML = poems[poem_num];
}